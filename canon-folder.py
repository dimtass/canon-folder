#!/usr/bin/python3

import sys
import os
import exifread
from shutil     import copyfile
from os         import listdir
from os.path    import isfile, join
from shutil     import move


root = "";
version = "0.1"

def moveFile (dir, file):
    directory = "./" + dir
    destination = directory + "/" + file

    if not os.path.exists(directory):
        os.makedirs(directory)
    print("Moving %s to %s..." % (file, directory))
    move(file, destination)
    # Check for xmp files and move them too
    xmp_file = file + ".xmp"
    if os.path.isfile(xmp_file):
        destination = directory + "/" + xmp_file
        print("Moving: %s to %s" % (xmp_file, directory))
        move(xmp_file, destination)

def extracData ():
    print("Start processing ...")
    fileList = [fl for fl in listdir(".") if isfile(join(".", fl))]
    for file in fileList:
        if file.endswith(".cr2") or file.endswith(".jpg") or file.endswith(".jpeg") or file.endswith(".png") or file.endswith(".CR2") or file.endswith(".JPEG") or file.endswith(".JPG") or file.endswith(".PNG"):
            tags = exifread.process_file(open(file, 'rb'))
            for tag in tags.keys():
                if tag in ('EXIF DateTimeOriginal'):
                    folder = str(tags[tag])
                    folder = folder[:10].replace(":", "_")
                    moveFile(folder, file)
    print("Finished")

def main():
    extracData()

if __name__ == "__main__":
    main()

canon-folder
----

If you're a Windows user you probably use the EOS utility to transfer images from
the camera to your computer. If you're a Linux user then you can use this python
script to `emulate` the EOS tool functionality.

As it is a very simplified script, you need to first to connect the camera to your
computer and then copy all the pictures to a folder (e.g. your `Pictures` folder).
In this case all the pictures will be copied in the same folder. It is convenient
though to split the photos in folders with `date` timestamp as the EOS tool does.

In this case copy this script to the same folder and run it like this:
```sh
./canon-folder
```

If you're also have done some editing and the editor created some `xmp` files for
each image then those will be copied too. I'm using `darktable` which by default
creates those file when you import them in the tool.

Finally, after the folders are created and the files are moved then you can backup
your files.
